﻿namespace WindowsFormsApp2
{
    partial class StartForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpenButton = new System.Windows.Forms.Button();
            this.OpenDialogButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.OpenParameterButton = new System.Windows.Forms.Button();
            this.OpenMdiButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OpenButton
            // 
            this.OpenButton.Location = new System.Drawing.Point(12, 142);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(75, 23);
            this.OpenButton.TabIndex = 0;
            this.OpenButton.Text = "OpenButton";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // OpenDialogButton
            // 
            this.OpenDialogButton.Location = new System.Drawing.Point(93, 142);
            this.OpenDialogButton.Name = "OpenDialogButton";
            this.OpenDialogButton.Size = new System.Drawing.Size(125, 23);
            this.OpenDialogButton.TabIndex = 1;
            this.OpenDialogButton.Text = "OpenDialogButton";
            this.OpenDialogButton.UseVisualStyleBackColor = true;
            this.OpenDialogButton.Click += new System.EventHandler(this.OpenDialogButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(224, 142);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 2;
            this.CloseButton.Text = "CloseButton";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // OpenParameterButton
            // 
            this.OpenParameterButton.Location = new System.Drawing.Point(306, 141);
            this.OpenParameterButton.Name = "OpenParameterButton";
            this.OpenParameterButton.Size = new System.Drawing.Size(126, 23);
            this.OpenParameterButton.TabIndex = 4;
            this.OpenParameterButton.Text = "OpenParameterButton";
            this.OpenParameterButton.UseVisualStyleBackColor = true;
            this.OpenParameterButton.Click += new System.EventHandler(this.OpenParameterButton_Click);
            // 
            // OpenMdiButton
            // 
            this.OpenMdiButton.Location = new System.Drawing.Point(439, 140);
            this.OpenMdiButton.Name = "OpenMdiButton";
            this.OpenMdiButton.Size = new System.Drawing.Size(105, 23);
            this.OpenMdiButton.TabIndex = 5;
            this.OpenMdiButton.Text = "OpenMdiButton";
            this.OpenMdiButton.UseVisualStyleBackColor = true;
            this.OpenMdiButton.Click += new System.EventHandler(this.OpenMdiButton_Click);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.OpenMdiButton);
            this.Controls.Add(this.OpenParameterButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.OpenDialogButton);
            this.Controls.Add(this.OpenButton);
            this.Name = "StartForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StartForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.Button OpenDialogButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button OpenParameterButton;
        private System.Windows.Forms.Button OpenMdiButton;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class TitleForm : Form
    {
        public TitleForm()
        {
            InitializeComponent();
        }
        public TitleForm(string s) : this()
        {
            this.Text = s;
        }
        private void TitleForm_Load(object sender, EventArgs e)
        {

        }

    }
}

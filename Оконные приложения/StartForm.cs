﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            Form form = new TitleForm();
            form.Show(); // запуск формы 
        }

        private void OpenDialogButton_Click(object sender, EventArgs e)
        {
            Form form = new TitleForm();
            form.ShowDialog(); // запуск диалоговой формы в модальном режиме
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void StartForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(
                "Вы хотите закрыть форму?",
                "Закрытие",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question
                );
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void OpenParameterButton_Click(object sender, EventArgs e)
        {
            Form form = new TitleForm(textBox1.Text);
            form.Show();
        }

        private void OpenMdiButton_Click(object sender, EventArgs e)
        {
            string s = ((Button)sender).Text;
            bool isOpen = false;
            foreach (var item in this.MdiChildren)
            {
                if (item.Text == s)
                {
                    isOpen = true;
                    item.Activate();
                    break;
                }
            }
            if (!isOpen)
            {
                Form form = new TitleForm(s);
                form.MdiParent = this;
                form.Show();
            }
        }
    }
}
